export class Feed{
    public id:string;
    public title:string;
    public text:string;

    constructor(id, title, text){
        this.id = id;
        this.title = title;
        this.text = text;
    }
}